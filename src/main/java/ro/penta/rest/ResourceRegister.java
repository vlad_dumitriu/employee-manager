package ro.penta.rest;

import org.glassfish.jersey.server.ResourceConfig;

public class ResourceRegister extends ResourceConfig {
	public ResourceRegister() {
		register(new DependencyBinder());
		packages(true, "ro.penta.rest");
	}

}
