package ro.penta.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ro.penta.model.Employee;
import ro.penta.service.EmployeeService;

@Path("/employee")
public class EmployeeWebServices {

	@Inject
	EmployeeService employeeService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Employee> listAll() {
		return employeeService.listAll();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Employee findById(@PathParam("id") int id) {
		return employeeService.findOneById(id);
	}

	@DELETE
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteEmployee(@PathParam("id") int id) {
		employeeService.delete(id);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void createEmployee(Employee emp) {
		employeeService.add(emp);
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateEmployee(Employee emp){
		employeeService.update(emp);
	}
}
