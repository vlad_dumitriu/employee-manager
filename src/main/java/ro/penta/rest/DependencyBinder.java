package ro.penta.rest;

import org.glassfish.hk2.utilities.binding.AbstractBinder;

import ro.penta.dao.DefaultEmployeeDAO;
import ro.penta.service.EmployeeService;

public class DependencyBinder extends AbstractBinder{

	@Override
	protected void configure() {
		bind(EmployeeService.class).to(EmployeeService.class);
		bind(DefaultEmployeeDAO.class).to(DefaultEmployeeDAO.class);
		
	}

	
}
