package ro.penta.dao;

import java.util.List;

import ro.penta.model.Employee;
/**
 *	DAO interface for the employee having standard DAO methods 
 *
 */
public interface EmployeeDAO {
	public void save(Employee emp);

	public Employee update(Employee emp);

	public int delete(int employeeId);

	public List<Employee> listAll();

	public Employee findOneById(int id);
	
	public List<Employee> findOneByName(String name);
}
