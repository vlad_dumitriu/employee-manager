package ro.penta.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import ro.penta.model.Employee;
import ro.penta.util.PersistenceUtil;

/**
 * Employee DAO implementation
 *
 */
public class DefaultEmployeeDAO implements EmployeeDAO {

	public void save(Employee emp) {
		this.getEntityManager().persist(emp);
	}

	public Employee update(Employee emp) {
		try {
			getEntityManager().getTransaction().begin();
			getEntityManager().merge(emp);
			getEntityManager().getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return emp;

	}

	public int delete(int id) {
		Employee emp = null;
		try {
			emp = getEntityManager().find(Employee.class, id);
			
			Employee toDelete = getEntityManager().merge(emp);
			getEntityManager().getTransaction().begin();
			getEntityManager().remove(toDelete);
			getEntityManager().getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

	@SuppressWarnings("unchecked")
	public List<Employee> listAll() {

		Query query = getEntityManager().createQuery("SELECT entity FROM Employee entity");
		List<Employee> employeeList = new ArrayList<Employee>();
		try {
			getEntityManager().getTransaction().begin();
			employeeList = query.getResultList();
			getEntityManager().getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return employeeList;
	}

	public Employee findOneById(int id) {
		Employee tempEmp = null;
		try {
			getEntityManager().getTransaction().begin();
			tempEmp = getEntityManager().find(Employee.class, id);
			getEntityManager().getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return tempEmp;
	}

	@SuppressWarnings("unchecked")
	public List<Employee> findOneByName(String name) {
		Query query = getEntityManager().createQuery("SELECT e FROM Employee e WHERE e.employeeName = '" + name + "'");
		List<Employee> employeeList = new ArrayList<Employee>();
		try {
			getEntityManager().getTransaction().begin();
			employeeList = query.getResultList();
			getEntityManager().getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return employeeList;
	}

	private EntityManager getEntityManager() {
		return PersistenceUtil.getEntityManager();
	}

}
