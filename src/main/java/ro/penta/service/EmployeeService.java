package ro.penta.service;

import java.util.List;

import javax.inject.Inject;

import ro.penta.dao.DefaultEmployeeDAO;
import ro.penta.model.Employee;
/**
 *	A service class which acts as an additional layer between the DAO and the Servlet classes 
 *
 */
public class EmployeeService {

	@Inject
	public DefaultEmployeeDAO entityDao;
	
	public void add(Employee emp){
		entityDao.save(emp);
	}

	public Employee update(Employee emp){
		return entityDao.update(emp);
	}

	public int delete(int employeeId1){
		 return entityDao.delete(employeeId1);
	}

	public List<Employee> listAll(){
		return entityDao.listAll();
	}

	public Employee findOneById(int id){
		return entityDao.findOneById(id);
	}
	
	public List<Employee> findOneByName(String name){
		return entityDao.findOneByName(name);
	}
}
