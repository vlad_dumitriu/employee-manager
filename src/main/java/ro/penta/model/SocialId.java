package ro.penta.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * SocialId class for holding the socialId number, in a OneToOne relationship with Employee
 *
 */
@Entity
public class SocialId {
	@Id
	@GeneratedValue
	private int socialidId;
	private String sociaidType;

	public int getSocialidId() {
		return socialidId;
	}

	public void setSocialidId(int socialidId) {
		this.socialidId = socialidId;
	}

	public String getSociaidType() {
		return sociaidType;
	}

	public void setSociaidType(String sociaidType) {
		this.sociaidType = sociaidType;
	}

	@Override
	public String toString() {
		return "SocialID [SocialId=" + socialidId + ", Socialid type=" + sociaidType + "]";
	}
}
