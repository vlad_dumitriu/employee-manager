package ro.penta.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * Class for handling an Employee, having relations with Department, SocialId
 * and Projects
 *
 */
@Entity
public class Employee {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int employeeId;

	private String employeeName;

	@ManyToOne
	private Department department;

	@OneToOne(cascade = CascadeType.ALL)
	private SocialId socialId;

	@ManyToMany
	private Set<Project> projects;

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public SocialId getSocialId() {
		return socialId;
	}

	public void setSocialId(SocialId socialId) {
		this.socialId = socialId;
	}

	public Set<Project> getProjects() {
		return projects;
	}

	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}

	public String toString() {
		return "Employees [Id=" + employeeId + ", Name=" + employeeName + ", Department=" + department + ", SocialId="
				+ socialId + ", Projects=" + projects + "]\n";
	}

}
