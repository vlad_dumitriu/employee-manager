package ro.penta.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Project class in a ManyToMany relationship with the Employee class
 *
 */
@Entity
public class Project {
	@Id
	@GeneratedValue
	private int projectId;
	private String projectName;

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	@Override
	public String toString() {
		return "Projects [Project ID:=" + projectId + ", Project Name:=" + projectName + "]\n";
	}
}
