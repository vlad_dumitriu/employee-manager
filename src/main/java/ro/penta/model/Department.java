package ro.penta.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
/**
 *	Department Entity which holds the Department Id and Name 
 *
 */
@Entity
public class Department {
	@Id
	@GeneratedValue
	private int departmentId;

	private String departmentName;

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	@Override
	public String toString() {
		return "Department [Id=" + departmentId + ", Name=" + departmentName + "]";
	}
}
