package ro.penta.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
/**
 *	Persistence Util class for creating an instance of the entitiManager 
 *
 */
public class PersistenceUtil {

	private static EntityManagerFactory emf= null;
	private static EntityManager em = null;

	public static synchronized EntityManager getEntityManager() {
		if (emf == null) {
			emf = Persistence.createEntityManagerFactory("employee-finder");
			em = emf.createEntityManager();
		}

		return em;
	}
}