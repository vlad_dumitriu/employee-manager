

var app = angular.module('EmployeeManager', []);
app.controller('EmployeeController', [ '$scope', '$http', function ($scope, $http) {


	$scope.loadEmployees = function(){
		$http.get('/employee-finder/rest/employee')
		.success(function(data) {
			$scope.employees = data;
		 })
		.error(function(data) {
		      alert('Error loading Employees');
		});
	};

	$scope.findEmployee = function(employee){
		$http.get('/employee-finder/rest/employee/'+employee.employeeId)
		.success(function(data) {
			$scope.employee = data;
			$scope.loadEmployees();
		 })
		.error(function(data) {
					alert('Error finding Employee');
		});
	};
	
	 	$scope.updateEmployee = function(employee) {
		   $scope.employee = employee;
		 }

		  $scope.insertEmployee = function() {
		   $scope.employee = null;
		}

	$scope.saveEmployee = function(){
		$http.post('/employee-finder/rest/employee')
		.success(function(data) {
			$scope.employee = data;
			$scope.loadEmployees();
		 })
		.error(function(data, status, headers, config) {
		      alert('Error saving Employee');
		});
	};

	$scope.deleteEmployee = function(employee){
		$http.delete('/employee-finder/rest/employee/'+employee.employeeId)
		.success(function(data, status, headers, config) {
			$scope.loadEmployees();
		 })
		.error(function(data, status, headers, config) {
		      alert('Error deleting Employee');
		});
	};

	$scope.editEmployee = function(){
		$http.put('/employee-finder/rest/employee/')
		.success(function(data, status, headers, config) {
			$scope.employee=data;
			$scope.loadEmployees();
		 })
		.error(function(data, status, headers, config) {
					alert('Error deleting Employee');
		});
	};

	$scope.loadEmployees();
}]);
