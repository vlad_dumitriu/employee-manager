### About ###
App that it's major role is to manage employees.

### Functional requests ###
* List all employees: localhost:8080/employee-finder/Employees
* Find by id one employee: localhost:8080/employee-finder/Employee?id=1
* Find by name one employee: localhost:8080/employee-finder/Employee?name=Vasile

### Unfunctional requests ###
* Delete by id
* Insert one employee

### Config ###
1. Install MySQL
2. Create a new database employeedb
3. In persistence.xml change your login details(user and password)
 